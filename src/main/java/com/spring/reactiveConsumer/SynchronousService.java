package com.spring.reactiveConsumer;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Service
public class SynchronousService {
    private final RestTemplate restTemplate;

    public SynchronousService(){
        this.restTemplate = new RestTemplateBuilder().build();
    }

    public void getAllProperties(){
        Property [] properties = restTemplate.
                getForEntity("http://localhost:8080/hotels", Property[].class)
                .getBody();

        Arrays.stream(properties).toList().forEach(property ->
                System.out.println(property.getName()));
    }
}
